package creds

import (
	"github.com/aws/aws-sdk-go/aws/credentials"
	"os/user"
	"path/filepath"
)
func CredsFromProfile(profileName string) (*credentials.Credentials, error) {
	usr, err := user.Current()
	if err != nil {
		return nil, err
	}
	credfile := filepath.Join(usr.HomeDir, ".aws", "credentials")
	creds := credentials.NewSharedCredentials(credfile, profileName)
	return creds, nil
}
